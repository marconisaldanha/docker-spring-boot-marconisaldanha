FROM openjdk:12
ADD target/docker-spring-boot-marconisaldanha.jar docker-spring-boot-marconisaldanha.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-marconisaldanha.jar"]
